﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Susie
{
    public static class ImagePlugin
    {
        [DllImport("loader.spi", CallingConvention=CallingConvention.StdCall)]
        public static extern int GetPicture(string filename, int len, uint flag, ref IntPtr pHBInfo, ref IntPtr pHBm, IntPtr lpProgressCallback, int IData);
    }
}
