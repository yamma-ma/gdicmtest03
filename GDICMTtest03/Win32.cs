﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
    public static class Memory
    {
        [DllImport("kernel32.dll")]
        public extern static IntPtr LocalAlloc(uint uFlags, uint uBytes);
        [DllImport("kernel32.dll")]
        public extern static IntPtr LocalLock(IntPtr hMem);
        [DllImport("kernel32.dll")]
        public extern static bool LocalUnlock(IntPtr hMem);
        [DllImport("kernel32.dll")]
        public extern static bool LocalFree(IntPtr hMem);
    }

    public static class GDI
    {
        public enum StretchMode : int
        {
            COLORONCOLOR = 3,
            HALFTONE     = 4
        };

        public enum Rop : uint
        {
            SRCCOPY = 0xcc0020
        };

        public enum DIBColorTableID : uint
        {
            DIB_RGB_COLORS = 0,
            DIB_PAL_COLORS = 1
        };

        /// <summary>
        /// ビットマップのヘッダー識別用
        /// </summary>
        public enum BitmapHeaderSize : uint
        {
            BITMAPCOREHEADER = 12,
            BITMAPINFOHEADER = 40,
            BITMAPV4HEADER = 108,
            BITMAPV5HEADER = 124
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CIEXYZ
        {
            public int X;
            public int Y;
            public int Z;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct CIEXYZTRIPLE
        {
            public CIEXYZ ciexyzRed;
            public CIEXYZ ciexyzGreen;
            public CIEXYZ ciexyzBlue;
        }
 
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct BITMAPV5HEADER
        {
            public BitmapHeaderSize bV5Size;
            public int bV5Width;
            public int bV5Height;
            public ushort bV5Planes;
            public ushort bV5BitCount;
            public uint bV5Compression;
            public uint bV5SizeImage;
            public int bV5XPelsPerMeter;
            public int bV5YPelsPerMeter;
            public uint bV5ClrUsed;
            public uint bV5ClrImportant;
            public uint bV5RedMask;
            public uint bV5GreenMask;
            public uint bV5BlueMask;
            public uint bV5AlphaMask;
            public uint bV5CSType;
            public CIEXYZTRIPLE bV5Endpoints;
            public uint bV5GammaRed;
            public uint bV5GammaGreen;
            public uint bV5GammaBlue;
            public uint bV5Intent;
            public uint bV5ProfileData;
            public uint bV5ProfileSize;
            public uint bV5Reserved;
        };

        [DllImport("gdi32.dll")]
        public extern static int SetStretchBltMode(IntPtr hdc, StretchMode iStretchMode);
        [DllImport("gdi32.dll",SetLastError=true)]
        public extern static int StretchDIBits(IntPtr hdc, int XDest, int YDest, int nDestWidth, int nDestHeight, int XSrc, int YSrc, int nSrcWidth, int nSrcHeight, IntPtr lpBits, IntPtr lpBitsInfo, DIBColorTableID iUsage, Rop dwRop);
    }

    public static class ICM
    {
        public enum ICMMode : int
        {
            ICM_OFF = 1,
            ICM_ON  = 2
        };

        [DllImport("gdi32.dll")]
        public extern static int SetICMMode(IntPtr hDC, ICMMode iEnableICM);
    }
}
