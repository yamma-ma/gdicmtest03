﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Susie;
using Win32;

namespace GDICMTtest03
{
    public partial class Form1 : Form
    {
        IntPtr infoHandle = IntPtr.Zero; // Susieプラグインから受け取るDIBのヘッダー部分のハンドル
        IntPtr infoPtr    = IntPtr.Zero;
        IntPtr bmpHandle  = IntPtr.Zero; // Susieプラグインから受け取るDIBのピクセルデータ部分のハンドル
        IntPtr bmpPtr     = IntPtr.Zero;

        GDI.BITMAPV5HEADER header; // ヘッダーの値を参照するために使用

        /// <summary>
        /// 画像を描画する範囲の矩形
        /// </summary>
        Rectangle paintRect
        {
            get
            {
                int offset = checkBox1.Height + checkBox1.Location.Y + checkBox1.Margin.Vertical  + checkBox1.Padding.Vertical + Padding.Top;

                return new Rectangle(0, offset, ClientSize.Width, ClientSize.Height - offset);
            }
        }

        public Form1()
        {
            InitializeComponent();

            Text = "GDICMTest03";
            AllowDrop = true;
            SetStyle(ControlStyles.Opaque, true);
            SetStyle(ControlStyles.ResizeRedraw, true);

            DragEnter += (sender, e) =>
            {
                e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Copy : DragDropEffects.None;
            };

            DragDrop += (sender, e) =>
            {
                string[] f = (string[])e.Data.GetData(DataFormats.FileDrop, false);

                open(f[0]);
            };

            checkBox1.CheckedChanged += (sender, e) =>
            {
                Invalidate(paintRect, false);
            };

            Paint += (sender, e) =>
            {
                Graphics g = e.Graphics;

                if (infoHandle == IntPtr.Zero || bmpHandle == IntPtr.Zero)
                {
                    // 画像未読み込みの場合は全体を塗りつぶす
                    g.FillRectangle(SystemBrushes.Control, DisplayRectangle);
                }
                else
                {
                    Rectangle rect = paintRect;

                    IntPtr hDC = g.GetHdc();

                    // ここがポイント！
                    ICM.SetICMMode(hDC, (checkBox1.Enabled && checkBox1.Checked) ? ICM.ICMMode.ICM_ON : ICM.ICMMode.ICM_OFF);

                    GDI.SetStretchBltMode(hDC, GDI.StretchMode.HALFTONE);
                    GDI.StretchDIBits(hDC, rect.X, rect.Y, rect.Width, rect.Height, 0, 0, header.bV5Width, header.bV5Height, bmpPtr, infoPtr, GDI.DIBColorTableID.DIB_RGB_COLORS, GDI.Rop.SRCCOPY);

                    g.ReleaseHdc();

                    g.FillRectangle(SystemBrushes.Control, new Rectangle(0, 0, DisplayRectangle.Width, paintRect.Top));
                }
            };
        }

        ~Form1()
        {
            if (infoHandle != IntPtr.Zero)
            {
                Memory.LocalFree(infoHandle);
            }
            if (bmpHandle != IntPtr.Zero)
            {
                Memory.LocalFree(bmpHandle);
            }
        }

        void open(string filename)
        {
            IntPtr ih = IntPtr.Zero;
            IntPtr bh = IntPtr.Zero;

            if (ImagePlugin.GetPicture(filename, 0, 0, ref ih, ref bh, IntPtr.Zero, 0) == 0)
            {
                Memory.LocalFree(infoHandle);
                Memory.LocalFree(bmpHandle);

                infoHandle = ih;
                bmpHandle = bh;

                infoPtr = Memory.LocalLock(ih);
                bmpPtr = Memory.LocalLock(bh);

                header = (GDI.BITMAPV5HEADER)Marshal.PtrToStructure(infoPtr, typeof(GDI.BITMAPV5HEADER));

                // 旧ヘッダーだった場合はICMのチェックボックスを無効にした上で変換を適用しない
                // （区別しやすくするための措置であり、必ずそうしなければならないわけではない）
                checkBox1.Enabled = (header.bV5Size == GDI.BitmapHeaderSize.BITMAPV5HEADER);

                Invalidate(paintRect, false);
            }
        }
    }
}
